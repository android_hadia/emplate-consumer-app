package emplate.android.com.emplate;

import java.util.Arrays;
import java.util.List;

import emplate.android.com.emplate.data.database.PostsEntry;

/**
 * Created by Hadia on 23/07/2018.
 */

public class TestData {

    static final PostsEntry POSTS_ENTRY = new PostsEntry(1,"posts","Shorts",true,"https://play.emplate.it/v6/posts/19545.html","2018-07-16T17:52:05+00:00", "2018-07-16T17:52:06+00:00",null,false);

    static final PostsEntry POST_ENTITY2 = new PostsEntry(2,"posts","Shorts",true,"https://play.emplate.it/v6/posts/19545.html","2018-07-16T17:52:05+00:00", "2018-07-16T17:52:06+00:00",null,true);
    ;

    static final List<PostsEntry> POSTS = Arrays.asList(POSTS_ENTRY, POST_ENTITY2);


}
