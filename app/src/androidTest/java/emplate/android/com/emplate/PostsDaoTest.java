package emplate.android.com.emplate;

/**
 * Created by Hadia on 23/07/2018.
 */

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import java.util.List;
import static org.hamcrest.Matchers.is;
import emplate.android.com.emplate.data.database.PostsDao;
import emplate.android.com.emplate.data.database.PostsDatabase;
import emplate.android.com.emplate.data.database.PostsEntry;
import static emplate.android.com.emplate.TestData.POSTS;
import static emplate.android.com.emplate.TestData.POST_ENTITY2;
/**
 * Test the implementation of {@link PostsDao}
 */
@RunWith(AndroidJUnit4.class)
public class PostsDaoTest {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private PostsDatabase mDatabase;

    private PostsDao mPostsDao;
    @Before
    public void initDb() throws Exception {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                PostsDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();

        mPostsDao = mDatabase.postsDao();
    }
    @After
    public void closeDb() throws Exception {
        mDatabase.close();
    }

    @Test
    public void getProductsWhenNoProductInserted() throws InterruptedException {
        List<PostsEntry> posts = LiveDataTestUtil.getValue(mPostsDao.getPosts());

        assertTrue(posts.isEmpty());
    }
    @Test
    public void getProductsAfterInserted() throws InterruptedException {
        mPostsDao.bulkInsert(POSTS);

        List<PostsEntry> posts = LiveDataTestUtil.getValue(mPostsDao.getPosts());
        assertThat(posts.size(), is(POSTS.size()));
    }
    @Test
    public void getSavedPosts() throws InterruptedException {
        mPostsDao.bulkInsert(POSTS);

        PostsEntry product = LiveDataTestUtil.getValue(mPostsDao.getSavedPosts()).get(0);

        assertThat(product.isSaved(), is(POST_ENTITY2.isSaved()));
        assertThat(product.getPostUniqueID(), is(POST_ENTITY2.getPostUniqueID()));

    }
}
