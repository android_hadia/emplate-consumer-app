package emplate.android.com.emplate.ui;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import emplate.android.com.emplate.data.PostRepository;
import emplate.android.com.emplate.data.database.PostsEntry;
import emplate.android.com.emplate.dependencies.AppInjector;

/**
 * Created by Hadia on 21/07/2018.
 */

public class PostsViewModel extends ViewModel{

    private LiveData<List<PostsEntry>> posts;
    private LiveData<List<PostsEntry>> savedPosts;
    @Inject
     PostRepository postsRepo;



    public PostsViewModel() {
        this.postsRepo = AppInjector.getAppComponent().getRepository();
    }
    public void init( ) {

        if (this.posts != null) {
            // ViewModel is created per Fragment so
            // we know the userId won't change
            return;
        }
        posts = postsRepo.getPosts();
    }

    public LiveData<List<PostsEntry>> getPosts() {

        return posts;
    }

    public LiveData<List<PostsEntry>> getCachedPosts() {

        return savedPosts;
    }


    public LiveData<List<PostsEntry>> getSavedPosts() {
        savedPosts=postsRepo.getSavedPosts();
        return savedPosts ;
    }
     public void savePost(PostsEntry postEntry)
     {
         postsRepo.savePost(postEntry);
     }

}
