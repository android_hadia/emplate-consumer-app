package emplate.android.com.emplate.data.database;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hadia on 21/07/2018.
 */

public class PeriodFieldConverter {
    private static Gson gson = new Gson();
    @TypeConverter
    public static List<Postperiod> stringToList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<Postperiod>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String ListToString(List<Postperiod> someObjects) {
        return gson.toJson(someObjects);
    }
}
