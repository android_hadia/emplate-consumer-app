/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package emplate.android.com.emplate.data.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * {@link Dao} which provides an api for all data operations with the {@link PostsDatabase}
 */
@Dao
public interface PostsDao {
    /**
     * Gets the saved posts
     * @return {@link LiveData} with saved post
     */
    @Query("SELECT * FROM posts WHERE saved = 1")
    LiveData<List<PostsEntry>> getSavedPosts();

    /**
     * Gets the all posts
     * @return {@link LiveData} with all post
     */
    @Query("SELECT * FROM posts")
    LiveData<List<PostsEntry>> getPosts();
    /**
     * Inserts a list of {@link PostsEntry} into the posts table. If there is a conflicting id
     * or date the posts entry uses the {@link OnConflictStrategy} of replacing the posts
     *  . The required uniqueness of these values is defined in the {@link PostsEntry}.
     *
     * @param posts A list of posts   to insert
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void bulkInsert(List<PostsEntry> posts);
    /**
     * Inserts of {@link PostsEntry} into the posts table. If there is a conflicting id
     * or date the posts entry uses the {@link OnConflictStrategy} of replacing the posts
     *  . The required uniqueness of these values is defined in the {@link PostsEntry}.
     *
     * @param post A list of posts   to insert
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void bulkInsert(PostsEntry post);


   /**
    * Deletes any unsaved posts
    *
    */
    @Query("DELETE FROM posts WHERE saved = 0")
    void deleteOldposts();

}
