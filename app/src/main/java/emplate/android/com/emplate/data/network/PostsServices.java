package emplate.android.com.emplate.data.network;

import java.util.List;

import emplate.android.com.emplate.data.database.PostsEntry;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by Hadia on 21/07/2018.
 */

public interface PostsServices {
    @Headers("X-Api-Key: $2y$10$KcrhlxTkD7/W6.YDhaePI.P8DCbv/9pmFcTH2JwfD1J16etNzUs0i")
    @GET("v3/posts?include=postfields,postperiods,thumbnail")
    Call<List<PostsEntry>> fetchPosts();
}
