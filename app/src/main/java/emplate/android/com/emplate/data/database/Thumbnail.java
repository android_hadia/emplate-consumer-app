package emplate.android.com.emplate.data.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Hadia on 21/07/2018.
 */

public class Thumbnail {

    @SerializedName("type")
    @Expose
    private String  thumbnailType;

    @SerializedName("id")
    @Expose
    private Integer thumbnailID;
    @SerializedName("name")
    @Expose
    private String thumbnailName;
    @SerializedName("filetype")
    @Expose
    private String filetype;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("url")
    @Expose
    private String thumbnailUrl;

    public Thumbnail() {
    }

    public String getThumbnailType() {
        return thumbnailType;
    }

    public void setThumbnailType(String thumbnailType) {
        this.thumbnailType = thumbnailType;
    }

    public Integer getThumbnailID() {
        return thumbnailID;
    }

    public void setThumbnailID(Integer thumbnailID) {
        this.thumbnailID = thumbnailID;
    }

    public String getThumbnailName() {
        return thumbnailName;
    }

    public void setThumbnailName(String thumbnailName) {
        this.thumbnailName = thumbnailName;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
