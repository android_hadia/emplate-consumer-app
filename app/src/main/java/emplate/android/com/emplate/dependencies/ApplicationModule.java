package emplate.android.com.emplate.dependencies;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import emplate.android.com.emplate.AppExecutors;
import emplate.android.com.emplate.data.PostRepository;
import emplate.android.com.emplate.data.database.PostsDao;
import emplate.android.com.emplate.data.database.PostsDatabase;

/**
 * Created by Hadia on 21/07/2018.
 */

@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }


    @Singleton
    @Provides
    Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    public PostsDatabase provideMyDatabase(Context context){
        return Room.databaseBuilder(context, PostsDatabase.class, PostsDatabase.DATABASE_NAME).fallbackToDestructiveMigration().allowMainThreadQueries().build();
    }

    @Singleton
    @Provides
    public PostsDao providePostsDaoProvider(PostsDatabase myDatabase){
        return myDatabase.postsDao();
    }

    @Singleton
    @Provides
    public PostRepository providePostRepositoryProvider( ){
        return new PostRepository();
    }


    @Singleton
    @Provides
    public AppExecutors provideAppExecutorsProvider( ){
        return  AppExecutors.getInstance();
    }

}
