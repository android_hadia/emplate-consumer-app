/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package emplate.android.com.emplate.data.database;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Defines the schema of a table in {@link Room} for a single Post
 * The post id is used as an {@link Index} so that its uniqueness can be ensured.
 */
@Entity(tableName = "posts", indices = {@Index(value = {"postUniqueID"}, unique = true)})
public class PostsEntry {


    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer postUniqueID;
    @SerializedName("type")
    @Expose
    private String postType;
    @SerializedName("name")
    @Expose
    private String postName;
    @SerializedName("approved")
    @Expose
    private Boolean approved;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;

    //    @Embedded
    @SerializedName("postfields")
    @Expose
    @TypeConverters(Converter.class)
    private List<Postfield> postFields ;

    @SerializedName("postperiods")
    @Expose
    @TypeConverters(PeriodFieldConverter.class)
    private List<Postperiod> postPeriods;

    @SerializedName("thumbnail")
    @Expose
    @Embedded
    private Thumbnail thumbnail;
    private boolean saved;


    public PostsEntry() {
    }
    @Ignore
    public PostsEntry(Integer postUniqueID, String postType, String postName, Boolean approved, String url, String createdAt, String updatedAt, String deletedAt, boolean saved) {
        this.postUniqueID = postUniqueID;
        this.postType = postType;
        this.postName = postName;
        this.approved = approved;
        this.url = url;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.saved = saved;
    }

    public Integer getPostUniqueID() {
        return postUniqueID;
    }

    public void setPostUniqueID(Integer postUniqueID) {
        this.postUniqueID = postUniqueID;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public List<Postfield> getPostFields() {
        return postFields;
    }

    public void setPostFields(List<Postfield> postFields) {
        this.postFields = postFields;
    }
    //
    public List<Postperiod> getPostPeriods() {
        return postPeriods;
    }

    public void setPostPeriods(List<Postperiod> postPeriods) {
        this.postPeriods = postPeriods;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }
}
