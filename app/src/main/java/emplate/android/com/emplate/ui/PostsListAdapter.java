package emplate.android.com.emplate.ui;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import agency.tango.android.avatarview.IImageLoader;
import agency.tango.android.avatarview.loader.PicassoLoader;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import emplate.android.com.emplate.R;
import emplate.android.com.emplate.data.database.PostsEntry;
import emplate.android.com.emplate.dependencies.AppInjector;

/**
 * Created by Hadia on 22/07/2018.
 */

public class PostsListAdapter extends RecyclerView.Adapter<PostsListAdapter. PostsAdapterViewHolder>  {
    private List<PostsEntry> mPostList;


    private PostsListAdapter.OnFavClickListener favClickListener;


    @Inject
    Context context;
    public PostsListAdapter( ) {
        context= AppInjector.getAppComponent().getContext();
        mPostList=new ArrayList<>();
    }


    public void setmPostList (List<PostsEntry> mPostList) {
        this.mPostList = mPostList;
    }


    public void setFavClickListener(OnFavClickListener favClickListener) {
        this.favClickListener = favClickListener;
    }


    @Override
    public int getItemCount() {
        if(mPostList!=null)
        return mPostList.size();
        else
            return 0;
    }

    @Override
    public PostsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item_layout, parent, false);

        return new PostsAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostsAdapterViewHolder holder, int position) {
        PostsEntry postResponse = mPostList.get(position);
        holder.postName.setText(postResponse.getPostName());
        IImageLoader imageLoader = new PicassoLoader();
        Picasso.with(context)
                .load(postResponse.getThumbnail().getThumbnailUrl())

                .into(holder.shopImage);

        if(!postResponse.getPostFields().isEmpty()&& postResponse.getPostFields().get(0).getPostFieldTypeId()==6)
        {
            if(!postResponse.getPostFields().get(0).getContent().isEmpty()) {
                bindPrices(holder, postResponse.getPostFields().get(0).getContent());
            }
        }

        if(!postResponse.getPostPeriods().isEmpty()) {
            bindExpiryValue(holder, postResponse.getPostPeriods().get(0).getStart(), postResponse.getPostPeriods().get(0).getStop());
        }
        holder.setFavClickListener(favClickListener);
        if(postResponse.isSaved())
        {
            holder.favIcon.setImageDrawable( ContextCompat.getDrawable(context, R.drawable.gem_slected_icon));

        }
        else
        {
            holder.favIcon.setImageDrawable( ContextCompat.getDrawable(context, R.drawable.gem_unslected_icon ));
        }
//        //holder.favIcon.setOnClickListener(this);
//        holder.favIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                holder.favIcon.setImageDrawable( ContextCompat.getDrawable(context, R.drawable.gem_slected_icon));
//            }
//        });
    }
    private void bindPrices (PostsAdapterViewHolder holder,String content  )
    {
        JSONObject jsonObj;
        try{
            if (content != null && content.length()>0) {
                jsonObj = new JSONObject(content);
                if (jsonObj.has("price")) {
                   String price = jsonObj.getString("price");
                   if(price!=null&& !price.isEmpty())
                   {
                       holder.priceNowTitle.setVisibility(View.VISIBLE);
                       holder. priceNowValue.setText(price);
                   }


                }
                if (jsonObj.has("price_before")) {
                    String priceBefore = jsonObj.getString("price_before");
                    if (priceBefore != null && !priceBefore.isEmpty()) {
                        holder.priceBeforeTitle.setVisibility(View.VISIBLE);
                        holder.priceBeforeValue.setText(priceBefore);
                    }
                }
                if (jsonObj.has("discount")) {
                    String discount = jsonObj.getString("discount");
                    if (discount != null && !discount.isEmpty()) {
                        holder.saveTitle.setVisibility(View.VISIBLE);
                        holder.saveValue.setText(discount);
                    }
                }
            }

        }catch (Throwable e){
            e.printStackTrace();
        }


    }

    private void bindExpiryValue (PostsAdapterViewHolder holder,String startDate,String stopDate  )
    {
        DateTime start = DateTime.parse(startDate);
        DateTime end = DateTime.parse(stopDate);
        Period difference = new Period(start, end, PeriodType.yearMonthDayTime());
        int months = difference.getMonths();
        int days =difference.getDays();
        int years =difference.getYears();
        String diffString ="";
        if(years!=0)
        {
            diffString=years +" "+context.getString(R.string.years);
        }
          if(months!=0)
        {
            diffString+=" "+months +" "+context.getString(R.string.month);
        }
         if(days!=0 )
        { diffString+=" "+days +" "+context.getString(R.string.days);
        }

        holder.expireValue.setText(diffString);

    }
    class PostsAdapterViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.shop_name)
         TextView shopName;
        @BindView(R.id.shop_image)
         ImageView shopImage;
        @BindView(R.id.post_name)
         TextView postName;
        @BindView(R.id.price_now_title)
         TextView priceNowTitle;
        @BindView(R.id.price_now_value)
         TextView priceNowValue;
        @BindView(R.id.price_before_title)
        TextView priceBeforeTitle;
        @BindView(R.id.price_before_value)
        TextView priceBeforeValue;
        @BindView(R.id.save_title)
        TextView saveTitle;
        @BindView(R.id.save_value)
        TextView saveValue;
        @BindView(R.id.expire_value)
        TextView expireValue;
        @BindView(R.id.fav_icon_img)
        ImageView favIcon;

        public long id;
        private PostsListAdapter.OnFavClickListener favClickListener;

        public void setFavClickListener(PostsListAdapter.OnFavClickListener favClickListener) {
            this.favClickListener = favClickListener;
        }

        PostsAdapterViewHolder(View view) {
            super(view);
             ButterKnife.bind(this,view);

        }




        @OnClick(R.id.fav_icon_img)
        public void onClick(View v) {

            int adapterPosition = getAdapterPosition();
            favClickListener.OnIconClickLisenter(v,adapterPosition);
//           = mPostList.get(adapterPosition);
//            if(mPostList.get(adapterPosition).get)
           // mClickHandler.onItemClick();
        }
    }

    public interface OnFavClickListener {

        void OnIconClickLisenter(View v, int position);



    }
}
