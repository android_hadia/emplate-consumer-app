package emplate.android.com.emplate.dependencies;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import emplate.android.com.emplate.AppExecutors;
import emplate.android.com.emplate.data.PostRepository;
import emplate.android.com.emplate.data.database.PostsDao;

/**
 * Created by Hadia on 21/07/2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    Context getContext();
    PostRepository getRepository();
    AppExecutors getAppExecutors();
    PostsDao getPostsDao();
}
