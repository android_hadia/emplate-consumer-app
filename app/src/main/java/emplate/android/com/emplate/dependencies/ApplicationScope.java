package emplate.android.com.emplate.dependencies;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Hadia on 21/07/2018.
 */


@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationScope {
}
