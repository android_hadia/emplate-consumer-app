package emplate.android.com.emplate.dependencies;

import javax.inject.Scope;

/**
 * Created by Hadia on 21/07/2018.
 */

@Scope
public @interface FlowScope {
}
