package emplate.android.com.emplate.ui;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import emplate.android.com.emplate.R;
import emplate.android.com.emplate.data.database.PostsEntry;
import emplate.android.com.emplate.dependencies.AppInjector;

/**
 * Created by Hadia on 21/07/2018.
 */

public class PostsFragment  extends LifecycleFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    @Inject
    Context  context;
    private static final String ARG_SECTION_NUMBER = "section_number";
    @BindView(R.id.recycler_view)
    RecyclerView postsListRecyclerView;
    private PostsListAdapter mPostsListAdapter;
    PostsViewModel  mViewModel;
    @BindView(R.id.pb_loading_indicator)
    ProgressBar mLoadingIndicator;
    @BindView(R.id.empty_state_layout)
    LinearLayout mEmptyState;


    public PostsFragment() {

        mPostsListAdapter= new PostsListAdapter();
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PostsFragment newInstance(int sectionNumber) {
        PostsFragment fragment = new PostsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context= AppInjector.getAppComponent().getContext();

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        postsListRecyclerView.setLayoutManager(mLayoutManager);
        postsListRecyclerView.setItemAnimator(new DefaultItemAnimator());



        postsListRecyclerView.setAdapter(mPostsListAdapter);
        showLoading();

        mViewModel  = ViewModelProviders.of(this).get(PostsViewModel.class);
        mViewModel.init();

        Bundle bundle=getArguments();

        //here is your list array
       int tabIndex=bundle.getInt(ARG_SECTION_NUMBER);
       switch (tabIndex)
       {
           case 0:
               loadAllPostsList();
               mPostsListAdapter.setFavClickListener(new PostsListAdapter.OnFavClickListener() {
                   @Override
                   public void OnIconClickLisenter(View v, int position) {
                       PostsEntry post =mViewModel.getPosts().getValue().get(position);

                       savePost( v, post);
                   }
               });
               break;
           case 1:
               loadSavedPostsList();
               mPostsListAdapter.setFavClickListener(new PostsListAdapter.OnFavClickListener() {
                   @Override
                   public void OnIconClickLisenter(View v, int position) {
                       PostsEntry post =mViewModel.getCachedPosts().getValue().get(position);
                       savePost( v, post);

                   }
               });
               break;
       }


        return rootView;
    }
    private void showLoading() {
        mLoadingIndicator.setVisibility(View.VISIBLE);
        postsListRecyclerView.setVisibility(View.INVISIBLE);
        mEmptyState.setVisibility(View.VISIBLE);
    }
    private void showPosts() {
        // First, hide the loading indicator
        mLoadingIndicator.setVisibility(View.INVISIBLE);
        postsListRecyclerView.setVisibility(View.VISIBLE);
        mEmptyState.setVisibility(View.INVISIBLE);
    }

    private void loadAllPostsList()
    {
        mViewModel.getPosts().observe(this,postResponses -> {
            mPostsListAdapter.setmPostList(postResponses);
            mPostsListAdapter.notifyDataSetChanged();
            if (postResponses != null && postResponses.size() != 0)
                showPosts();
        });
    }
    private void loadSavedPostsList()
    {
        mViewModel.getSavedPosts().observe(this,postResponses -> {
            mPostsListAdapter.setmPostList(postResponses);
            mPostsListAdapter.notifyDataSetChanged();
            if (postResponses != null && postResponses.size() != 0) {
                showPosts();
            }
            else
            {
                mLoadingIndicator.setVisibility(View.INVISIBLE);
                mEmptyState.setVisibility(View.VISIBLE);
            }

        });
    }



    private void  savePost(View v,PostsEntry post)
    {
        if(!post.isSaved())
        {
            ((ImageView) v).setImageDrawable( ContextCompat.getDrawable(context, R.drawable.gem_slected_icon));
            post.setSaved(true);
        }
        else
        {
            ((ImageView) v).setImageDrawable( ContextCompat.getDrawable(context, R.drawable.gem_unslected_icon));
            post.setSaved(false);
        }

        mViewModel.savePost(post);
    }
}
