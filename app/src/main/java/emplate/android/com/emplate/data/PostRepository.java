package emplate.android.com.emplate.data;

import android.arch.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import emplate.android.com.emplate.AppExecutors;
import emplate.android.com.emplate.data.database.PostsDao;
import emplate.android.com.emplate.data.database.PostsEntry;
import emplate.android.com.emplate.data.network.ApiFactory;
import emplate.android.com.emplate.data.network.PostsServices;
import emplate.android.com.emplate.dependencies.AppInjector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hadia on 21/07/2018.
 */
@Singleton
public class PostRepository {
    @Inject
    AppExecutors appExecutors;

    @Inject
    PostsDao postsDao;

    public PostRepository() {
        appExecutors= AppInjector.getAppComponent().getAppExecutors();
        postsDao =AppInjector.getAppComponent().getPostsDao();

        ApiFactory.createService(PostsServices.class).fetchPosts().enqueue(new Callback<List<PostsEntry>>() {
            @Override
            public void onResponse(Call<List<PostsEntry>> call, Response<List<PostsEntry>> response) {

                appExecutors.diskIO().execute(() -> {

                    // Deletes old historical data
                    deleteOldData();

                    postsDao.bulkInsert(response.body());


                    //    // Insert our new weather data into Sunshine's database
                    //   mWeatherDao.bulkInsert(newForecastsFromNetwork);
                    //  Log.d(LOG_TAG, "New values inserted");
                });


            }

            @Override
            public void onFailure(Call<List<PostsEntry>> call, Throwable t) {

            }
        });

    }

    public LiveData<List<PostsEntry>> getPosts() {
        // This is not an optimal implementation, we'll fix it below
        return postsDao.getPosts();
    }
    public LiveData<List<PostsEntry>> getSavedPosts() {
        // This is not an optimal implementation, we'll fix it below
        return postsDao.getSavedPosts();
    }
    public void savePost(PostsEntry post) {
         postsDao.bulkInsert(post);
    }


    private void deleteOldData() {

        postsDao.deleteOldposts();
    }
}
