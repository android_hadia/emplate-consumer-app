package emplate.android.com.emplate.dependencies;

import android.util.Log;

/**
 * Created by Hadia on 21/07/2018.
 */

public class AppInjector {

    private static ApplicationComponent mAppComponent;

    public static ApplicationComponent getAppComponent() {
        Log.d("Injection", "get: " + mAppComponent);
        return mAppComponent;
    }

    public static void setComponent(ApplicationComponent appComponent) {
        Log.d("Injection", "create: " + appComponent);
        mAppComponent = appComponent;
    }
}
