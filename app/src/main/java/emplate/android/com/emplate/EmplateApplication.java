package emplate.android.com.emplate;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

import emplate.android.com.emplate.dependencies.AppInjector;
import emplate.android.com.emplate.dependencies.ApplicationComponent;
import emplate.android.com.emplate.dependencies.ApplicationModule;
import emplate.android.com.emplate.dependencies.DaggerApplicationComponent;


/**
 * Created by Hadia on 21/07/2018.
 */

public class EmplateApplication extends Application {


    public void onCreate() {
        super.onCreate();

        JodaTimeAndroid.init(this);
        ApplicationComponent appComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        AppInjector.setComponent(appComponent);

    }
}
